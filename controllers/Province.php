<?php
namespace Mvc\Controller;

require_once '../app/Controller.php';
require_once '../models/Province.php';

/**
*
*/



class Province extends \Mvc\App\Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->_model = new \Mvc\Model\Province;
    }
    public function index($page = 1)
    {
        $rows = $this->_model->get($page);
        require '../views/province/index.php';
        // var_dump($rows);
    }

    public function remember($province)
    {
        $_SESSION['provincia'] = $province;
        header('Location: /province/index');
    }


    public function seek()
    {
        require '../views/province/seek.php';
    }
    public function found()
    {
        $rows = $this->_model->getById($_POST['provincia']);
        require '../views/province/found.php';
    }

    public function delete($id = 0)
    {
        $result = $this->_model->delete($id);
        require '../views/province/delete.php';
    }
}
