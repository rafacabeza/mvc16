<?php

/**
* Modelo ActiveRecord Articulo
*/
namespace Mvc\Model;
dddd
dddddd
class Articulo
{
    const DB_HOST = "localhost";
    const DB_USER = "usuario";
    const DB_PASS = "usuarioXX";
    const DB_DATABASE = "mvc";

    private $_db;
    private $_rows;

    public function __construct()
    {
        // echo "Modelo Articulo";
        $this->_db = new \mysqli($this::DB_HOST, $this::DB_USER, $this::DB_PASS, $this::DB_DATABASE);
        if ($this->_db->connect_errno > 0) {
            //die($mysqli->error);
            throw new \Exception("Error de conexión a la BBDD" . '<br>' . $this->_db->connect_error  , 404);
            
        }
    }

    public function get()
    {
        $sql = "SELECT * FROM articulo";
        $result = $this->_db->query($sql);

        if ($this->_db->errno) {
            die($this->_db->error);
        }

        $this->_rows = array();
        while ($row = $result->fetch_assoc()) {
            $this->_rows[] = $row;
        }
    }

    public function getById($id)
    {
        $sql = "SELECT * FROM articulo where id=$id";
        $result = $this->_db->query($sql);

        if ($this->_db->errno) {
            die($this->_db->error);
        }

        while ($row = $result->fetch_assoc()) {
            return $row; //devuelve la primera fila
        }
        return array(); //si el id no existe array vacia
    }

    /**
     * Gets the value of _rows.
     *
     * @return mixed
     */
    public function getRows()
    {
        return $this->_rows;
    }

    public function delete($id)
    {
        $sql = "delete from articulo where id = $id";
        $this->_db->query($sql);
        if ($this->_db->errno > 0) {
            die($this->_db->error . '<br>' . $sql);
        }
    }

    public function new()
    {
        $sql = "insert into articulo (nombre, precio) values('$_POST[nombre]', $_POST[precio])";
        $this->_db->query($sql);
        if ($this->_db->errno > 0) {
            die($this->_db->error . '<br>' . $sql);
        }
    }
    public function update()
    {
        $sql = "update  articulo set nombre='$_POST[nombre]', precio=$_POST[precio] where id=$_POST[id]";
        $this->_db->query($sql);
        if ($this->_db->errno > 0) {
            die($this->_db->error . '<br>' . $sql);
        }
    }
}
